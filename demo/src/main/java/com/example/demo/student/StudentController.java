package com.example.demo.student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@RestController
@RequestMapping(path = "api/v1/student")

    public class StudentController {

    //we are using dependency injection
    @Autowired
    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    //get
    //accessing to the Service layer to get students
    @GetMapping
    public List<Student> getStudents(){
        return studentService.getStudents();
    }

    //post
    @PostMapping
    public void registerNewStudent(@RequestBody Student student){
        //so we take the request body and then we map it into this student in the argument
        studentService.addNewStudent(student);
    }

    //we are deleting a students by theirs ids
    @DeleteMapping(path = "{studentId}")
    public void deleteStudent(@PathVariable("studentId") Long studentId){
        studentService.deleteStudent(studentId);
    }


    @PutMapping(path = "{studentId}")
    public void updateStudent(
        @PathVariable("studentId") Long studentId,
        @RequestParam(required = false) String name,
        @RequestParam(required = false) String email)
    {
        studentService.updateStudent(studentId, name, email);
    }
}
