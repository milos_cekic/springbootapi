package com.example.demo.student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

//this is a bean
//this class is a service class
@Service
public class StudentService {

    private final StudentRepository studentRepository;

    @Autowired
    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    //using repository in data access layer we want to get all students
    public List<Student> getStudents(){
        return studentRepository.findAll();
    }

    //we want to add new student in a database
    public void addNewStudent(Student student) {
       //business logic is that there are unique email addresses
        Optional<Student> studentOptional =
        studentRepository.findStudentByEmail(student.getEmail());

        //if there is such a student we need to throw exception
        if(studentOptional.isPresent())
            throw new IllegalStateException("Email taken!");

        //we are adding student
        studentRepository.save(student);
    }

    public void deleteStudent(Long studentId) {

        boolean exists = studentRepository.existsById(studentId);

        if(!exists)
            throw new IllegalStateException("Student with id "+studentId + "does not exists!");

        //if it exists we want to delete the student
        studentRepository.deleteById(studentId);

    }

    //with this annotation entity goes into a managed state

    @Transactional
    public void updateStudent(Long studentId, String name, String email) {

        Student student = studentRepository.findById(studentId)
                .orElseThrow(() -> new IllegalStateException(
                        "Student with id "+studentId+" does not exists!"
                ));

        if(name!=null && name.length() > 0 && !Objects.equals(student.getName(), name))
            student.setName(name);

        if(email != null && email.length()>0 && !Objects.equals(student.getEmail(), email)) {
            Optional<Student> studentOptional = studentRepository.findStudentByEmail(email);

            if (studentOptional.isPresent())
                throw new IllegalStateException("Email taken!");

            student.setEmail(email);
        }
    }
}
