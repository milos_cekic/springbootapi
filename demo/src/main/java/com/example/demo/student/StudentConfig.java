package com.example.demo.student;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@Configuration
public class StudentConfig {

    @Bean
    CommandLineRunner commandLineRunner(StudentRepository repository){
        //we want to save these students into the db
        return args -> {

         Student milos = new Student(
                    "Milos",
                    "milos.@gmail.com",
                    LocalDate.of(2000, Month.JANUARY, 5));

        Student jelena = new Student("Jelena",
                "jelena@gmail.com",
                LocalDate.of(2001, Month.JANUARY, 5));

        //when we invoke saveAll hibernate write sql
        repository.saveAll(
                List.of(milos, jelena)
        );
    };
    }

}
